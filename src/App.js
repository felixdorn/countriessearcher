import React, { useState, useEffect } from "react";
import Countries from "./components/Countries";
import Pagination from "./components/Pagination";
import SearchBar from "./components/SearchBar";
import axios from "axios";
import "./App.css";

const App = () => {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [countriesPerPage] = useState(20);
  const [query, setQuery] = useState("");
  const [error, setError] = useState("");
  useEffect(() => {
    if (query.length === 0) {
      const fetchPosts = async () => {
        setLoading(true);
        setError("Chargement...");
        await axios
          .get("https://restcountries.eu/rest/v2/all")
          .then(res => {
            setCountries(res.data);
            setLoading(false);
          })
          .catch(() => {
            setError("Une erreur est survenue ! Réessayez plus tard !");
          });
      };
      fetchPosts();
    } else {
      const fetchQuery = async () => {
        setLoading(true);
        await axios
          .get(
            "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20190705T173704Z.c54151c43497ae1b.3afd6df02283c0d45c41932da730b23e105a82ca&lang=fr-en&text=" +
              query
          )
          .then(async res => {
            await axios
              .get("https://restcountries.eu/rest/v2/name/" + res.data.text[0])
              .then(res => {
                setCountries(res.data);
                setLoading(false);
              })
              .catch(() => {
                setCountries([]);
                setError("Aucun pays trouvé !");
              });
          });
      };

      fetchQuery();
    }
  }, [query]);

  // Get current posts
  const indexOfLastCountry = currentPage * countriesPerPage;
  const indexOfFirstCountry = indexOfLastCountry - countriesPerPage;
  const currentCountries = countries.slice(
    indexOfFirstCountry,
    indexOfLastCountry
  );
  const handleSearch = inputQuery => {
    setQuery(inputQuery);
  };
  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className="container mt-5">
      <h1 className="text-primary mb-3">
        <a href="/" alt="Acceuil">
          Pays du monde
        </a>
      </h1>
    <p>Les données viennent de <a href="https://restcountries.eu/">restcountries.eu</a>. Les traductions viennent de <a href="https://yandex.ru/">yandex.net</a>. Développé par Félix Dorn</p>
      <SearchBar handler={handleSearch} />
      <span>
        <b>{countries.length} </b>pays{" "}
        {countries.length > 1 ? "trouvés" : "trouvé"}.
      </span>
      <Countries error={error} countries={currentCountries} loading={loading} />
      <Pagination
        countriesPerPage={countriesPerPage}
        totalCountries={countries.length}
        paginate={paginate}
      />
    </div>
  );
};

export default App;
