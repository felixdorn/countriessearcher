import React, { Component } from "react";

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = { query: "" };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.props.handler(event.target.value);
    this.setState({ query: event.target.value });
  }
  render() {
    return (
      <div className="form-group">
        <input
          value={this.state.value}
          onChange={this.handleChange}
          className="form-control"
          type="search"
          placeholder="Commencez à taper le nom d'un pays..."
        />
      </div>
    );
  }
}
