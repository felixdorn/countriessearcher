import React from "react";
const Countries = ({ error, countries, loading }) => {
  if (loading) {
    return (
      <ul className="list-group mb-4">
        <li className="list-group-item">{error}</li>
      </ul>
    );
  }
  const getPrettyName = country => {
    if (country.name === country.translations.fr) {
      return <h5 className="card-title">{country.name}</h5>;
    } else {
      return (
        <h5 className="card-title">
          {country.translations.fr} - &nbsp;
          <span className="text-muted">{country.name}</span>
        </h5>
      );
    }
  };
  const getCurrencies = country => {
    let html = [];
    let currencies = [];

    country.currencies.forEach(currencie => {
      currencies.push(
        <li>
          -&nbsp;&nbsp;&nbsp;
          {currencie.name} ({currencie.symbol})
        </li>
      );
    });
    html.push(
      <p>
        <h6> {currencies.length > 1 ? "Monnaies" : "Monnaie"} </h6>
        <ul className="list-unstyled">{currencies}</ul>
      </p>
    );
    return html;
  };
  const getLanguages = country => {
    let html = [];
    if (country.languages.length === 1) {
      country.languages.forEach(language => {
        html.push(
          <p>
            <h6>Langue parlée</h6>
            <ul className="list-unstyled">
              <li>-&nbsp;&nbsp;&nbsp;{language.name}</li>
            </ul>
          </p>
        );
      });
    } else {
      let languages = [];
      country.languages.forEach(language => {
        languages.push(<li>-&nbsp;&nbsp;&nbsp;{language.name}</li>);
      });

      html.push(
        <p>
          <h6>Langues parlées </h6>
          <ul className="list-unstyled">{languages}</ul>
        </p>
      );
    }
    return html;
  };
  const getArea = country => {
    return (
      <p>
        <h6>Superficie</h6>
        <ul className="list-unstyled">
          <li>
            -&nbsp;&nbsp;&nbsp;
            {new Intl.NumberFormat("fr-FR").format(country.area)}km²
          </li>
        </ul>
      </p>
    );
  };
  const getCapital = country => {
    return (
      <p>
        <h6>Capitale</h6>
        <ul className="list-unstyled">
          <li>
            -&nbsp;&nbsp;&nbsp;
            {country.capital}
          </li>
        </ul>
      </p>
    );
  };
  const getTime = country => {
    let html = [];

    html.push(
      <p>
        <h6>Heure locale</h6>
        <ul className="list-unstyled">
          <li>
            -&nbsp;&nbsp;&nbsp;
            {country.timezones[0]}
          </li>
        </ul>
      </p>
    );
    return html;
  };
  return (
    <section className="cards mt-4">
      {countries.map(country => (
        <div key={country.name} className="card country">
          <a href={country.flag} title="Cliquez pour agrandir">
            <img
              className="card-img-top flag"
              src={country.flag}
              alt={`Drapeau d'un pays appelé ${country.translations.fr}`}
            />
          </a>
          <div className="card-body">
            <p className="card-text">
              {getPrettyName(country)}
              <h6 className="card-subtitle mb-2 text-muted">
                {new Intl.NumberFormat("fr-FR").format(country.population)}
                &nbsp;habitants
              </h6>
            </p>
            {getArea(country)}
            {getCapital(country)}
            {getLanguages(country)}
            {getCurrencies(country)}
            {getTime(country)}
            <br />
          </div>
          <div className="card-footer">
            <a
              className="btn btn-primary card-link"
              target="_blank"
              rel="noopener noreferrer"
              href={`https://fr.wikipedia.org/wiki/${country.translations.fr}`}
            >
              Découvrir plus
            </a>
            <a
              className="btn btn-link card-link"
              target="_blank"
              rel="noopener noreferrer"
              href={`http://maps.google.com/?q=${country.translations.fr}`}
            >
              Voir sur la carte
            </a>
          </div>
        </div>
      ))}
    </section>
  );
};

export default Countries;
